# [JavaScipt Essential Training - lynda.com](https://www.lynda.com/JavaScript-tutorials/JavaScript-Essential-Training/574716-2.html)

## Chapter 02 - The basics

### Add inline JavaScript to a HTML document
* JavaScript loading methods
    * right away loading `<script src="script.js"></script>`
    * asynchronous loading `<script src="script.js async"></script>`
    * deferred loading `<script src="script.js" defer></script>`

### How to write JavaScript
* JavaScript is case sensitve
* Use camelCase
* JavaScript doesn't care about whitespace or ending each statement with a semicolon, but we should

## Chapter 03 - Working with data

### Variables
* Variable name can contain letter, number, underscore ( _ ) or dollar sign ($)
* Can't make a variable starting with a number

### Data types
* Data types in JavaScript
    * numeric
    * string
    * boolean
    * null
    * undefined
* Find the datatype, use: `typeof variableName`

### Arithmetic operators and math
* Arithmetic operators: +, -, \*, /, +=, -=, \*=, /=
* Unary operators: ++, --

### Condition and logic
* Operators: && (AND), || (OR)
* JavaScript doesn't have XOR operator, so we can make it manually: A XOR B => `(A || B) && (A != B)`
* Ternary operator: `(condition) ? true : false`

### Properties and mothoes in arrays
* Properties:
    * `length`: number of elements
* Methods:
    * `reverse()`: reverse the array
    * `shift()`: remove the first value of the array
    * `pop()`: Remove the last value of the array:
    * `unshift(<comma-separated list>)`: add comma-separated list of values to the front of the array
    * `push(<comma-separated list>)`: add comma-separated list to the end of the array
    * `splice(pos, n, <comma-separated list>)`: add/remove elements to/from array, return remove elements as array
        * `pos`: start position to remove
        * `n`: number of elements to remove
        * `<comma-separated list>`: the list to be added to array at position `pos`
    * `slice(start, end)`: return the selected elements as a new array (select elements from `start` to `end-1`)
    * `indexOf(search, fromIndex)`: return the first element that matches the `search` parameter, from `fromIndex`
    * `join(separator)`: return the items in an array as a comma separated string

## Chapter 04 - Functions and Objects

### Functions in JavaScript
Three types of functions:
* Named Functions
* Anonymous functions
* Immediately invoked function expressions

### Named Functions
Example
```javascript
function findMax(a, b) {
    return a > b ? a : b;
}
```
To call function: `findMax(5, 2)`

### Anonymous functions
Example
```javascript
var theMax = function(a, b) {
    return a > b ? a : b;
}
```
To call function: `theMax(5, 2)`

### Immediately invoked function expressions
Example
```javascript
var max = (function(a, b) {
    return a > b ? a : b;
})(5, 2)
```
Variable `max` contain the result

### Variable Scope
* Global variable: be declared at the root of the script, can be use anywhere in the script
* Local variable: be declared in the block or the function, can be use in this Scope
* If a variable be declared without `var` prefix, it'll become a global variable

### Let and Const
* Use `const const_name = value` to declare a constant
* Difference from `var` and `let`

Script 1:

```javascript
var a = 5;
{
	let a = 6;
    console.log('a in block: ', a);
}
console.log('a out of block: ', a);
```
The result:
```
a in block:  6
a out of block:  6
```

Script 2:

```javascript
var a = 5;
{
	let a = 6;
    console.log('a in block: ', a);
}
console.log('a out of block: ', a);
```
The result:
```
a in block:  6
a out of block:  5
```

### Object constructors
Example
```javascript
function Person(name, age) {
    this.name = name;
    this.age = age;
}
```
Create a Person object: `var person = new Person('Perter', 20);`

### Sidebar: Dot and bracket notation
`person['name']` is similar to `person.name`, but in somes cacse, we must to use bracket notation


### Closures
Can be defined: a function inside a function, that relies on variables in the outside function to work
Example
```javascript
function getEms(pixels) {
    var baseValue = 16;

    function calculate() {
        return pixels / baseValue;
    }
    return calculate;
}

var smallSize = getEms(12);
var mediumSize = getEms(18);
```

## Chapter 05 -  JavaScript and the DOM, Part 1: Changing DOM elements

### DOM: The document object model
* BOM: Browser Object Model => `window`
* DOM: Document Object Model => `window.document` or call directly `document`

### Target elements in the DOM with querySelector methods
* Properties
    * `document.body`: the body element
    * `document.title`: the document title
    * `document.URL`: the document URL
* Methods
    * `document.getElementById('ID')`: get the element with a specified ID
    * `document.getElementsByClassName('className')`: get all elements with a specified class name as array
    * `document.getElementsByTagName('htmlTag')`: get all elements with a specified HTML Tag as array
    * `document.querySelector('comma-separated CSS selectors')`: get the first element matching specified selector(s)
    * `document.querySelectorAll('comma-separated CSS selectors')`: get all elements matching specified selector(s)

### Access and change elements
* `element.innerHTML`: get/set the string contain the text inside the tag
* `element.outerHTML`: get/set the string contain tag and inside text
* `element.id`: get/set element id
* `element.className`: get/set the string contain all class names
* `element.classList`: get the DOMTokenList as an array contain all class

### Access and change classes (methods of classList)
* `add('class')`
* `remove(class)`
* `item(index)` or `classList[index]`
* `toggle('class')`
* `contains('class')`

### Access and change attributes (properties and methods of element)
* `attributes`
* `hasAttribute('attribute')`
* `getAttribute('attribute')`
* `setAttribute('attribute', 'value')`
* `removeAttribute('attribute')`

### Add DOM elements
1. Create the element
    * `var childNode = createElement('tag')`
2. Create the text node that goes inside the element
    * `var textNode = createTextNode('text')`
3. Add the text node to the element
    * `childNode.appendChild(textNode)`
4. Add the element to the DOM tree
    * `parentNode.appendChild(childNode)`

### Apply inline CSS to an element
* `style`: return a dict all possible properties could be applied using inline CSS, then we can use this to get/set value of each CSS property as a dict. We also assign `style` to a string that decribes CSS style
* If the CSS property has a hyphen in name (background-color), it becomes camelCase in JavaScript (backgroundColor)
* If using JavaScript to apply style changes, it's better to create a custom CSS rule targeting class selectors, and adding or removing these classes from element

## Chapter 05 -  JavaScript and the DOM, Part 2: Events

### Typical DOM events?
* Browser-level events (load, window resize, etc...)
* DOM-level events (click, focus, submit...)

### Trigger functions with event handlers
* `element.<event> = trigger-func`

### Add and use event listeners
* Event handler can handle a single event
* So, we can event listeners: `element.addEventListener('event', function, useCapture)`
    * `useCapture`: `true` or `false`

### Pass arguments via event listners
* If we want to call a function with arguments, we have to call it in an anonymous function
* The function used for `addEventListener` will automatically have `this` bound to the current element
* Example
```javascript
//e is an Event Object
element.addEventListener('event', function(e){
    someFunc(someVar);
    this.doSth();
});
```

## Chapter 08 - Project: Typing Speed Tester

## Interval
* Create interval: `var interval = setInterval(trigger-func, time-in-milisecond)`
* Clear interval: `clearInterval(interval)`

## Chapter 09 - Loop
* `for`, `while`, `do while` similar to loops in `C/C++`
* can use `break`, `continue`

## Chapter 11 - Troubleshooting, Validating, and Minifying JavaScript

* Use debugger of browse
* Use console: `console.log()`, `console.info()`,
 `console.error()`...
* Online script linting: [jshint](http://jshint.com/)

### Automate script linting
* Use ESLint
* Install ESLint: `npm install -g eslint`
* Init ESLint in project: `eslint --intit` (should save the config file as JavaScript)
* Install ESLint package in Atom
* If want disable the Error: Unexpected console statement, add `"no-console": 0` to the `rules` of config file

### Online script minification
* [Minifier](http://www.minifier.org/)

function getEms(pixels) {
    var baseValue = 16;

    function calculate() {
        return pixels / baseValue;
    }
    return calculate;
}

var smallSize = getEms(12);
var mediumSize = getEms(18);

console.log(smallSize());
console.log(mediumSize());
